# desktop.mjdev.eu

Gnome shell desktop extension which add possibility to have web or static html/css/javascript widgets on desktop.
Idea is that desktop can contain more than folders and icons.
Switchable desktop background with daily used links, rss feeds, widgets.